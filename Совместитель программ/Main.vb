﻿Public Class Main
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Timer1.Enabled = True
        Button1.Enabled = False
        CheckedListBox1.Enabled = False
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If ProgressBar1.Value = 50 Then
            Timer1.Enabled = False
            For i As Integer = 0 To CheckedListBox1.Items.Count - 1
                SaveSetting("PragrammSovmestitel", "Activ", i + 1, CheckedListBox1.GetItemChecked(i))
            Next
            MsgBox("Готово!", , "Информация")
            CheckedListBox1.Enabled = True
            Button1.Enabled = True
            ProgressBar1.Value = 0
        Else
            ProgressBar1.Value += 1
        End If
    End Sub
    Private Sub CheckedListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckedListBox1.SelectedIndexChanged
        ProgressBar1.Value = 0
        Button1.Enabled = True
    End Sub
    Private Sub Main_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        For i As Integer = 0 To CheckedListBox1.Items.Count - 1
            CheckedListBox1.SetItemChecked(i, GetSetting("PragrammSovmestitel", "Activ", i + 1, False))
        Next
    End Sub
End Class
